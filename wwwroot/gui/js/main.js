var projjs = (function() {
  //Global variables

  //DOM elements
  var elements = {
    main: document.getElementsByTagName("main")[0]
  };

  // Public object
  var Public = {};

  // Page-specific public functions (if any)
  Public.example = function() {};

  // Private functions

  var toggleMainNavigation = function() {
    var mainMenu = document.querySelector(".toggleMainMenu");
      mainMenu.addEventListener("click", function () {
        var expanded = this.getAttribute("aria-expanded");
        var targetId = this.getAttribute("aria-controls");
        var target = document.getElementById(targetId);

        this.setAttribute(
          "aria-expanded",
          expanded === "true" ? "false" : "true"
        );

        target.classList.toggle("navigation--open");
        this.classList.toggle("toggleMainMenu--expanded");
      });
  };




  var toggleSubNavigation = function() {
    var subMenu = document.querySelectorAll(".togglebtn");
    if (subMenu) {
      for (var i = 0; i < subMenu.length; i++) {
        addEvL(subMenu[i]);
      }
    }
    function addEvL(el) {
      el.addEventListener("click", function () {
        var expanded = this.getAttribute("aria-expanded");
        var targetId = this.getAttribute("aria-controls");
        var target = document.getElementById(targetId);
        this.setAttribute("aria-expanded", expanded === "true" ? "false" : "true");
        target.classList.toggle("open");
        this.classList.toggle("togglebtn--expanded");
      });
    }
  };






  /** Initiate functions  */
  var init = (function() {
    toggleMainNavigation();
    toggleSubNavigation();
  })();

  return Public;
})((window.projjs = window.projjs || {}));




